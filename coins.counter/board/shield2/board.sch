EESchema Schematic File Version 4
LIBS:board-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5900 2900 0    50   Input ~ 0
SCL
Text GLabel 5900 2800 0    50   Input ~ 0
SDA
Text GLabel 5900 2700 0    50   Input ~ 0
GND
Text GLabel 5900 2600 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x22_Female J?
U 1 1 5DA8E5C0
P 6200 3400
F 0 "J?" H 6228 3380 50  0001 L CNN
F 1 "Conn_01x22_Female" H 6228 3335 50  0001 L CNN
F 2 "" H 6200 3400 50  0001 C CNN
F 3 "~" H 6200 3400 50  0001 C CNN
	1    6200 3400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x22_Female J?
U 1 1 5DA9143E
P 6700 3500
F 0 "J?" H 6592 2467 50  0001 C CNN
F 1 "Conn_01x19_Female" H 6592 2466 50  0001 C CNN
F 2 "" H 6700 3500 50  0001 C CNN
F 3 "~" H 6700 3500 50  0001 C CNN
	1    6700 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 2700 6000 2700
Wire Wire Line
	6000 2800 5900 2800
Wire Wire Line
	5900 2900 6000 2900
Wire Wire Line
	6900 2600 7050 2600
Wire Wire Line
	6900 2400 7050 2400
Text GLabel 9250 3400 2    50   Input ~ 0
D16
Text GLabel 9250 3300 2    50   Input ~ 0
D14
Text GLabel 9250 3200 2    50   Input ~ 0
D15
Text GLabel 9250 3100 2    50   Input ~ 0
A0
Text GLabel 9250 3000 2    50   Input ~ 0
A1
Text GLabel 9250 2900 2    50   Input ~ 0
A2
Text GLabel 9250 2800 2    50   Input ~ 0
A3
Text GLabel 7050 2600 2    50   Input ~ 0
RST
Text GLabel 7050 2400 2    50   Input ~ 0
RAW
Wire Wire Line
	9250 3300 6900 3300
Wire Wire Line
	6000 2600 5900 2600
Wire Wire Line
	6900 3400 9250 3400
Text GLabel 8900 1800 1    50   Input ~ 0
S2E
Text GLabel 9000 1750 1    50   Input ~ 0
ST
Text GLabel 7050 2500 2    50   Input ~ 0
GND
Text GLabel 8300 4650 3    50   Input ~ 0
M+
Text GLabel 8700 4650 3    50   Input ~ 0
M-
Wire Wire Line
	8300 4650 8300 4250
$Comp
L Device:D D?
U 1 1 5DCA9552
P 8500 4250
F 0 "D?" H 8500 4466 50  0000 C CNN
F 1 "D" H 8500 4375 50  0000 C CNN
F 2 "" H 8500 4250 50  0001 C CNN
F 3 "~" H 8500 4250 50  0001 C CNN
	1    8500 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 4250 8300 4250
Connection ~ 8300 4250
Wire Wire Line
	8650 4250 8700 4250
Connection ~ 8700 4250
Wire Wire Line
	6900 2500 7050 2500
Wire Wire Line
	6900 3600 8700 3600
Wire Wire Line
	8700 3600 8700 4250
Wire Wire Line
	6900 3700 8300 3700
Wire Wire Line
	8300 3700 8300 4250
Wire Wire Line
	6900 3200 9250 3200
Wire Wire Line
	6900 3100 9250 3100
Text GLabel 8800 1800 1    50   Input ~ 0
S50C
Wire Wire Line
	5900 2400 6000 2400
Wire Wire Line
	5900 2500 6000 2500
Text GLabel 5900 2500 0    50   Input ~ 0
RX
Text GLabel 5900 2400 0    50   Input ~ 0
TX
Text GLabel 9250 2700 2    50   Input ~ 0
VCC
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DA92CC8
P 2300 5100
F 0 "J?" V 2408 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 2363 4912 50  0001 R CNN
F 2 "" H 2300 5100 50  0001 C CNN
F 3 "~" H 2300 5100 50  0001 C CNN
	1    2300 5100
	0    -1   -1   0   
$EndComp
Text GLabel 2300 4700 1    50   Input ~ 0
GND
Wire Wire Line
	2300 4700 2300 4900
$Comp
L Device:R_Small R?
U 1 1 5DA95DF9
P 2400 4650
F 0 "R?" H 2341 4604 50  0001 R CNN
F 1 "R_Small" H 2341 4695 50  0001 R CNN
F 2 "" H 2400 4650 50  0001 C CNN
F 3 "~" H 2400 4650 50  0001 C CNN
	1    2400 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DA9736B
P 2400 4300
F 0 "RV?" H 2340 4346 50  0001 R CNN
F 1 "1E" H 2341 4300 50  0000 R CNN
F 2 "" H 2400 4300 50  0001 C CNN
F 3 "~" H 2400 4300 50  0001 C CNN
	1    2400 4300
	1    0    0    -1  
$EndComp
Text GLabel 2400 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	2400 4900 2400 4750
Wire Wire Line
	2400 4550 2400 4400
Wire Wire Line
	2400 4200 2400 4000
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAAD6F8
P 2700 5100
F 0 "J?" V 2808 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 2763 4912 50  0001 R CNN
F 2 "" H 2700 5100 50  0001 C CNN
F 3 "~" H 2700 5100 50  0001 C CNN
	1    2700 5100
	0    -1   -1   0   
$EndComp
Text GLabel 2700 4700 1    50   Input ~ 0
GND
Wire Wire Line
	2700 4700 2700 4900
$Comp
L Device:R_Small R?
U 1 1 5DAAD701
P 2800 4650
F 0 "R?" H 2741 4604 50  0001 R CNN
F 1 "R_Small" H 2741 4695 50  0001 R CNN
F 2 "" H 2800 4650 50  0001 C CNN
F 3 "~" H 2800 4650 50  0001 C CNN
	1    2800 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAAD707
P 2800 4300
F 0 "RV?" H 2740 4346 50  0001 R CNN
F 1 "1E" H 2741 4300 50  0000 R CNN
F 2 "" H 2800 4300 50  0001 C CNN
F 3 "~" H 2800 4300 50  0001 C CNN
	1    2800 4300
	1    0    0    -1  
$EndComp
Text GLabel 2800 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	2800 4900 2800 4750
Wire Wire Line
	2800 4550 2800 4400
Wire Wire Line
	2800 4200 2800 4000
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAAF1CD
P 3050 5100
F 0 "J?" V 3158 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 3113 4912 50  0001 R CNN
F 2 "" H 3050 5100 50  0001 C CNN
F 3 "~" H 3050 5100 50  0001 C CNN
	1    3050 5100
	0    -1   -1   0   
$EndComp
Text GLabel 3050 4700 1    50   Input ~ 0
GND
Wire Wire Line
	3050 4700 3050 4900
$Comp
L Device:R_Small R?
U 1 1 5DAAF1D6
P 3150 4650
F 0 "R?" H 3091 4604 50  0001 R CNN
F 1 "R_Small" H 3091 4695 50  0001 R CNN
F 2 "" H 3150 4650 50  0001 C CNN
F 3 "~" H 3150 4650 50  0001 C CNN
	1    3150 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAAF1DC
P 3150 4300
F 0 "RV?" H 3090 4346 50  0001 R CNN
F 1 "1E" H 3091 4300 50  0000 R CNN
F 2 "" H 3150 4300 50  0001 C CNN
F 3 "~" H 3150 4300 50  0001 C CNN
	1    3150 4300
	1    0    0    -1  
$EndComp
Text GLabel 3150 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	3150 4900 3150 4750
Wire Wire Line
	3150 4550 3150 4400
Wire Wire Line
	3150 4200 3150 4000
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAB10B3
P 3400 5100
F 0 "J?" V 3508 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 3463 4912 50  0001 R CNN
F 2 "" H 3400 5100 50  0001 C CNN
F 3 "~" H 3400 5100 50  0001 C CNN
	1    3400 5100
	0    -1   -1   0   
$EndComp
Text GLabel 3400 4700 1    50   Input ~ 0
GND
Wire Wire Line
	3400 4700 3400 4900
$Comp
L Device:R_Small R?
U 1 1 5DAB10BC
P 3500 4650
F 0 "R?" H 3441 4604 50  0001 R CNN
F 1 "R_Small" H 3441 4695 50  0001 R CNN
F 2 "" H 3500 4650 50  0001 C CNN
F 3 "~" H 3500 4650 50  0001 C CNN
	1    3500 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAB10C2
P 3500 4300
F 0 "RV?" H 3440 4346 50  0001 R CNN
F 1 "1E" H 3441 4300 50  0000 R CNN
F 2 "" H 3500 4300 50  0001 C CNN
F 3 "~" H 3500 4300 50  0001 C CNN
	1    3500 4300
	1    0    0    -1  
$EndComp
Text GLabel 3500 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	3500 4900 3500 4750
Wire Wire Line
	3500 4550 3500 4400
Wire Wire Line
	3500 4200 3500 4000
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAB2E35
P 3750 5100
F 0 "J?" V 3858 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 3813 4912 50  0001 R CNN
F 2 "" H 3750 5100 50  0001 C CNN
F 3 "~" H 3750 5100 50  0001 C CNN
	1    3750 5100
	0    -1   -1   0   
$EndComp
Text GLabel 3750 4700 1    50   Input ~ 0
GND
Wire Wire Line
	3750 4700 3750 4900
$Comp
L Device:R_Small R?
U 1 1 5DAB2E3E
P 3850 4650
F 0 "R?" H 3791 4604 50  0001 R CNN
F 1 "R_Small" H 3791 4695 50  0001 R CNN
F 2 "" H 3850 4650 50  0001 C CNN
F 3 "~" H 3850 4650 50  0001 C CNN
	1    3850 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAB2E44
P 3850 4300
F 0 "RV?" H 3790 4346 50  0001 R CNN
F 1 "1E" H 3791 4300 50  0000 R CNN
F 2 "" H 3850 4300 50  0001 C CNN
F 3 "~" H 3850 4300 50  0001 C CNN
	1    3850 4300
	1    0    0    -1  
$EndComp
Text GLabel 3850 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	3850 4900 3850 4750
Wire Wire Line
	3850 4550 3850 4400
Wire Wire Line
	3850 4200 3850 4000
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAB50D4
P 4100 5100
F 0 "J?" V 4208 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 4163 4912 50  0001 R CNN
F 2 "" H 4100 5100 50  0001 C CNN
F 3 "~" H 4100 5100 50  0001 C CNN
	1    4100 5100
	0    -1   -1   0   
$EndComp
Text GLabel 4100 4700 1    50   Input ~ 0
GND
Wire Wire Line
	4100 4700 4100 4900
$Comp
L Device:R_Small R?
U 1 1 5DAB50DD
P 4200 4650
F 0 "R?" H 4141 4604 50  0001 R CNN
F 1 "R_Small" H 4141 4695 50  0001 R CNN
F 2 "" H 4200 4650 50  0001 C CNN
F 3 "~" H 4200 4650 50  0001 C CNN
	1    4200 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAB50E3
P 4200 4300
F 0 "RV?" H 4140 4346 50  0001 R CNN
F 1 "1E" H 4141 4300 50  0000 R CNN
F 2 "" H 4200 4300 50  0001 C CNN
F 3 "~" H 4200 4300 50  0001 C CNN
	1    4200 4300
	1    0    0    -1  
$EndComp
Text GLabel 4200 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	4200 4900 4200 4750
Wire Wire Line
	4200 4550 4200 4400
Wire Wire Line
	4200 4200 4200 4000
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAB7A37
P 4450 5100
F 0 "J?" V 4558 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 4513 4912 50  0001 R CNN
F 2 "" H 4450 5100 50  0001 C CNN
F 3 "~" H 4450 5100 50  0001 C CNN
	1    4450 5100
	0    -1   -1   0   
$EndComp
Text GLabel 4450 4700 1    50   Input ~ 0
GND
Wire Wire Line
	4450 4700 4450 4900
$Comp
L Device:R_Small R?
U 1 1 5DAB7A40
P 4550 4650
F 0 "R?" H 4491 4604 50  0001 R CNN
F 1 "R_Small" H 4491 4695 50  0001 R CNN
F 2 "" H 4550 4650 50  0001 C CNN
F 3 "~" H 4550 4650 50  0001 C CNN
	1    4550 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAB7A46
P 4550 4300
F 0 "RV?" H 4490 4346 50  0001 R CNN
F 1 "1E" H 4491 4300 50  0000 R CNN
F 2 "" H 4550 4300 50  0001 C CNN
F 3 "~" H 4550 4300 50  0001 C CNN
	1    4550 4300
	1    0    0    -1  
$EndComp
Text GLabel 4550 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	4550 4900 4550 4750
Wire Wire Line
	4550 4550 4550 4400
Wire Wire Line
	4550 4200 4550 4000
Text GLabel 2050 3000 0    50   Input ~ 0
S1E
Text GLabel 2050 3100 0    50   Input ~ 0
S20C
Text GLabel 2050 3200 0    50   Input ~ 0
S5C
Text GLabel 2050 3300 0    50   Input ~ 0
S10C
Text GLabel 2050 3400 0    50   Input ~ 0
S2C
Text GLabel 2050 3500 0    50   Input ~ 0
S1C
Wire Wire Line
	2050 3000 2200 3000
Wire Wire Line
	2050 3500 4000 3500
Wire Wire Line
	2050 3400 3650 3400
Wire Wire Line
	2050 3300 3300 3300
Wire Wire Line
	2050 3200 2950 3200
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAEDE56
P 4800 5100
F 0 "J?" V 4908 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 4863 4912 50  0001 R CNN
F 2 "" H 4800 5100 50  0001 C CNN
F 3 "~" H 4800 5100 50  0001 C CNN
	1    4800 5100
	0    -1   -1   0   
$EndComp
Text GLabel 4800 4700 1    50   Input ~ 0
GND
Wire Wire Line
	4800 4700 4800 4900
$Comp
L Device:R_Small R?
U 1 1 5DAEDE5E
P 4900 4650
F 0 "R?" H 4841 4604 50  0001 R CNN
F 1 "R_Small" H 4841 4695 50  0001 R CNN
F 2 "" H 4900 4650 50  0001 C CNN
F 3 "~" H 4900 4650 50  0001 C CNN
	1    4900 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAEDE64
P 4900 4300
F 0 "RV?" H 4840 4346 50  0001 R CNN
F 1 "1E" H 4841 4300 50  0000 R CNN
F 2 "" H 4900 4300 50  0001 C CNN
F 3 "~" H 4900 4300 50  0001 C CNN
	1    4900 4300
	1    0    0    -1  
$EndComp
Text GLabel 4900 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	4900 4900 4900 4750
Wire Wire Line
	4900 4550 4900 4400
Wire Wire Line
	4900 4200 4900 4000
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5DAF09E5
P 5150 5100
F 0 "J?" V 5258 4912 50  0001 R CNN
F 1 "Conn_01x03_Male" V 5213 4912 50  0001 R CNN
F 2 "" H 5150 5100 50  0001 C CNN
F 3 "~" H 5150 5100 50  0001 C CNN
	1    5150 5100
	0    -1   -1   0   
$EndComp
Text GLabel 5150 4700 1    50   Input ~ 0
GND
Wire Wire Line
	5150 4700 5150 4900
$Comp
L Device:R_Small R?
U 1 1 5DAF09ED
P 5250 4650
F 0 "R?" H 5191 4604 50  0001 R CNN
F 1 "R_Small" H 5191 4695 50  0001 R CNN
F 2 "" H 5250 4650 50  0001 C CNN
F 3 "~" H 5250 4650 50  0001 C CNN
	1    5250 4650
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Small RV?
U 1 1 5DAF09F3
P 5250 4300
F 0 "RV?" H 5190 4346 50  0001 R CNN
F 1 "1E" H 5191 4300 50  0000 R CNN
F 2 "" H 5250 4300 50  0001 C CNN
F 3 "~" H 5250 4300 50  0001 C CNN
	1    5250 4300
	1    0    0    -1  
$EndComp
Text GLabel 5250 4000 1    50   Input ~ 0
VCC
Wire Wire Line
	5250 4900 5250 4750
Wire Wire Line
	5250 4550 5250 4400
Wire Wire Line
	5250 4200 5250 4000
Wire Wire Line
	2200 4900 2200 3000
Connection ~ 2200 3000
Wire Wire Line
	2200 3000 6000 3000
Wire Wire Line
	2600 4900 2600 3100
Wire Wire Line
	2050 3100 2600 3100
Connection ~ 2600 3100
Wire Wire Line
	2600 3100 6000 3100
Wire Wire Line
	2950 4900 2950 3200
Connection ~ 2950 3200
Wire Wire Line
	2950 3200 6000 3200
Wire Wire Line
	3300 4900 3300 3300
Connection ~ 3300 3300
Wire Wire Line
	3300 3300 6000 3300
Wire Wire Line
	3650 4900 3650 3400
Connection ~ 3650 3400
Wire Wire Line
	3650 3400 6000 3400
Wire Wire Line
	4000 4900 4000 3500
Connection ~ 4000 3500
Wire Wire Line
	4000 3500 6000 3500
Wire Wire Line
	6900 3000 8800 3000
Wire Wire Line
	6900 2800 9000 2800
Wire Wire Line
	6900 2700 9250 2700
Wire Wire Line
	9000 1750 9000 2100
Connection ~ 9000 2800
Wire Wire Line
	9000 2800 9250 2800
Wire Wire Line
	8900 1800 8900 2000
Wire Wire Line
	6900 2900 8900 2900
Connection ~ 8900 2900
Wire Wire Line
	8900 2900 9250 2900
Wire Wire Line
	8800 1800 8800 1900
Connection ~ 8800 3000
Wire Wire Line
	8800 3000 9250 3000
Wire Wire Line
	5050 2100 9000 2100
Wire Wire Line
	5050 2100 5050 4900
Connection ~ 9000 2100
Wire Wire Line
	9000 2100 9000 2800
Wire Wire Line
	4700 2000 8900 2000
Wire Wire Line
	4700 2000 4700 4900
Connection ~ 8900 2000
Wire Wire Line
	8900 2000 8900 2900
Wire Wire Line
	4350 1900 8800 1900
Wire Wire Line
	4350 1900 4350 4900
Connection ~ 8800 1900
Wire Wire Line
	8800 1900 8800 3000
Wire Wire Line
	8700 4250 8700 4650
Wire Wire Line
	6900 3500 9250 3500
Text GLabel 9250 3500 2    50   Input ~ 0
D10
$EndSCHEMATC
