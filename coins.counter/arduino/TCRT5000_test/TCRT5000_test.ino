/* http://www.bajdi.com
   TCRT5000 sensor connected to Arduino Uno
   TCRT5000 pins: (see datasheet http://www.vishay.com/docs/83760/tcrt5000.pdf )
     C = Arduino digital pin 2
     E = GND
     A = 100 ohm resistor, other end of resistor to 5V
     C = GND

 Digital PIN
                      +------+   +---------------+
  +          +--------+100 Oh+---+5K variable pot+------+ 5V
  |          |        +------+   +---------------+
  |          |
+-+----------+--+
|               +-+
| TOP VIEW        |
|                 |
|               +-+
+-+----------+--+
  |          |
  |          |
+-+----------+------------------------------------------+ GND

*/

const int tcrtPin = 12;     // connected to the TCRT5000 C pin
const int led =  13;      // the number of the LED pin
const int buzzer =  6;

// variables will change:
int tcrtState = 0;         // variable for reading the TCRT5000 status

void setup() {
  // initialize the LMED pin as an output:
  pinMode(led, OUTPUT);      
  pinMode(buzzer, OUTPUT);      
  // initialize the tcrt5000 pin as an input, and turn on the internal pullup resistors:
  pinMode(tcrtPin, INPUT); 
  digitalWrite(tcrtPin, HIGH);  

  Serial.begin(57600);
}

void loop(){
  // read the state of the tcrt5000:
  tcrtState = digitalRead(tcrtPin);

  

  // check if the tcrt5000 sensor detects something.
  // if it is, the tcrtState is LOW:
  if (tcrtState == LOW) {     
    // turn LED on:    
    digitalWrite(led, HIGH);  
    Serial.println(tcrtState);
    tone(buzzer, 1000);
    delay(100);
    noTone(buzzer);
    
  } 
  else {
    // turn LED off:
    digitalWrite(led, LOW); 
  }
}
